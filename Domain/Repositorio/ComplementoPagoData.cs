﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositorio
{
    public class ComplementoPagoData : IDisposable
    {
       
        private SqlConnection _db = null;
        private string cadenaConexion = null;

        public ComplementoPagoData(string idCompany)
        {
            ObtenerCredencialesDeConexion(idCompany);
        }

        /// <summary>
        /// Obtiene la informaciño básica de la factura con el UUID proporcionado
        /// </summary>
        /// <param name="uuidRelated">UUID relacionado a la factura</param>
        /// <returns>Objeto de tipo CfdiInformacionGpObtener_Result con el docnumber y amount de la factura</returns>
        public CfdiInformacionGpObtener_Result GetCfdiInformacion(string uuidRelated)
        {
            CfdiInformacionGpObtener_Result result;
            var queryText = "SELECT TOP 1 (FD.serie+FD.folio) AS DocNumber," +
                            "FD.total AS Amount," +
                            "FD.uuid AS UUIDRelated" +
                            " FROM dbo.Fiel_documento_cfdi FD" +
                            " WHERE  FD.uuid LIKE \'%@uuidRelated%\'";
            try
            {
                using (_db = new SqlConnection(cadenaConexion))
                {
                    _db.Open();
                    SqlCommand query = new SqlCommand(queryText, _db);
                    query.Parameters.Add("@uuidRelated", SqlDbType.VarChar).Value = uuidRelated;
                    
                    var reader = query.ExecuteReader();
                    reader.Read();
                    result = new CfdiInformacionGpObtener_Result
                    {
                        DocNumber = reader["DocNumber"].ToString(),
                        Amount = (decimal) reader["Amount"],
                        UUIDRelated = reader["UUIDRelated"].ToString()
                    };
                }
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Dispose();
            }
            
        }

        /// <summary>
        /// Consulta el estatus del proveedor al momento de la consulta 
        /// </summary>
        /// <param name="idProveedor">identificador del proveedor de tipo cadena</param>
        /// <returns>Booleano representativo del estatus del proveedor</returns>
        public bool ValidarProveedor(string idProveedor)
        {
            bool valido = false;
            var queryText= "SELECT TOP 1  CONVERT(BIT,PV.VENDSTTS) estatus" +
                           " FROM dbo.PM00200 PV" +
                           " WHERE  PV.VENDORID LIKE '%@idProveedor%'";
            try
            {
                using (_db = new SqlConnection(cadenaConexion))
                {
                    _db.Open();
                    SqlCommand query = new SqlCommand(queryText, _db);
                    query.Parameters.Add("@idProveedor", SqlDbType.VarChar).Value = idProveedor;

                    valido = (bool) query.ExecuteScalar();

                }

                return valido;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Dispose();
            }

        }

        /// <summary>
        /// Obtiene la información del proveedor relacionada al RFC
        /// </summary>
        /// <param name="rfc">RFC se usa como identificador del proveedor</param>
        /// <returns>IEnumerable tipo ProveedorPorRfcObtener_Result, la idea es que regrese un solo 
        /// registro con la información del proveedor</returns>
        public IEnumerable<ProveedorPorRfcObtener_Result> GetProveerdor(string rfc)
        {
            List<ProveedorPorRfcObtener_Result> lstProveedores = new List<ProveedorPorRfcObtener_Result>();
            var queryText = "SELECT  RTRIM(PV.VENDORID) ProviderId, " +
                            " RTRIM(PV.VENDNAME)	ProviderName, " +
                            " RTRIM(PV.TXRGNNUM) RFC, " +
                            " RTRIM(PV.COMMENT2) Email " +
                            " FROM dbo.PM00200 PV " +
                            " WHERE PV.VENDSTTS =1 AND PV.TXRGNNUM LIKE ''%'+ @rfc+'%'''";
            try
            {
                _db.Open();
                SqlCommand query = new SqlCommand(queryText, _db);
                query.Parameters.Add("@rfc", SqlDbType.VarChar).Value = rfc;
                var result = query.ExecuteReader();
                
                while (result.Read())
                {
                    lstProveedores.Add(new ProveedorPorRfcObtener_Result
                    {
                        ProviderId = result["ProviderId"].ToString(),
                        ProviderName = result["ProviderName"].ToString(),
                        RFC = result["RFC"].ToString(),
                        Email = result["Email"].ToString(),
                    });
                }
                return lstProveedores;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Dispose();
            }

        }


        /// <summary>
        /// Método encargado de formar la cadena de conexión, dependiendo de la empresa
        /// </summary>
        /// <param name="idCompany">Identificador de la empresa</param>
        private void ObtenerCredencialesDeConexion(string idCompany)
        {
            short companyId = 0;
            Int16.TryParse(idCompany, out companyId);
            using (ComplementoPagoDBEntities dba = new ComplementoPagoDBEntities())
            {
                var obj = (from i in dba.Empresa
                    where i.idEmpresa == companyId
                    select i).FirstOrDefault();
                if (obj != null)
                {
                    cadenaConexion = string.Format(
                        "Persist Security Info=False;User ID={0};Password={1};Initial Catalog={2};Server={3}",
                        obj.usuario, obj.contrasena, obj.Nombre,
                        obj.IpServer + (string.IsNullOrEmpty(obj.Instancia) ? "" : "\\" + obj.Instancia));
                    
                }
            }
            
        }


        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
