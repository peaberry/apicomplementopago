﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Xml;
using System.Xml.Linq;
using ApiComplementoPago.Models;
using ApiComplementoPago.ValidadorCfdiReference;
using Domain;
using Microsoft.Ajax.Utilities;

namespace ApiComplementoPago.Controllers
{
    public class CFDIProviderHeaderController : ApiController
    {
        private static readonly ComplementoPagoDBEntities _bd = new ComplementoPagoDBEntities();
        private List<CFDIProvider> lstEstatusComprobantes = new List<CFDIProvider>();
        private int _companyId=0;
        private string _contratoApiProdigia;
        private string _usuarioApiProdigia;
        private string _passwordApiProdigia;

        /// <summary>
        /// Consulta el listado de CFDIs correspondientes a los complementos de pago enviados en el objeto
        /// “CfdiPaymentHeader”, comprueba la validez de los documentos ante el SAT con servicio de PRODIGIA
        /// </summary>
        /// <param name="pModel"></param>
        /// <returns>regresa un listado de documentos validados con sus correspondientes facturas,
        /// en caso de error la propiedad “ValidationMessage” contiene el error</returns>
        [HttpPost]
        //[EnableCors(origins: "http://10.1.5.130", headers: "*", methods: "*")]
        public IEnumerable<CFDIProvider> Validate(CFDIProviderHeaderModel pModel)
        {
            
            InicializarCredenciales(pModel.CompanyId);
            foreach (var pago in pModel.Payments)
            {
                ////TEST
                //var bytes = File.ReadAllText(@"C:\\Users\\Iveth Franco\\Desktop\\Evidencias\\COMPLEMENTO DE PAGO\\xmls\\oslo\\RUQ840702D15_000E014734.xml");
                //pago.XMLBase64 = bytes; //Convert.ToBase64String(bytes);
                //END_TEST
                var bytes = Convert.FromBase64String(pago.XMLBase64);
                pago.XMLBase64 = Encoding.UTF8.GetString(bytes);
                var item = new CFDIProvider();
                item.UUIDRelated = pago.UUIDRelated;
                item.PaymentAmount = pago.PaymentAmount;
                item.Moneda = pago.Moneda;

                ObtenerInformacionCfdiGp(item);
                var monto = pModel.Payments
                                .Where(p => p.UUIDRelated == pago.UUIDRelated)
                                .Sum(pmt => pmt.PaymentAmount);

                // Si el valor absoluto de la diferencia es menor o igual a 10 para moneda mexicana o 15 en dolares, se toma como pago completo
                item.IsPaymentComplete = item.Moneda == "MXN" 
                                            ? Math.Abs(item.Amount - monto) <= 10
                                            : Math.Abs(item.Amount - monto) <= 15;
                
                ValidarXmlApi(pago.XMLBase64, item);

                lstEstatusComprobantes.Add(item);
            }
           
            return lstEstatusComprobantes;
        }

        private void InicializarCredenciales(string idCompany)
        {
            Int32.TryParse(idCompany, out _companyId );
            var obj = (from i in _bd.Empresa
                where i.idEmpresa == _companyId
                select i).FirstOrDefault();
            if (obj != null)
            {
                _contratoApiProdigia = obj.Contrato;
                _passwordApiProdigia = obj.pass_sat;
                _usuarioApiProdigia = obj.user_sat;   
            }
        }

        private void ObtenerInformacionCfdiGp(CFDIProvider item)
        {
            try
            {
                var infoFactura = _bd.CfdiInformacionGpObtener(item.UUIDRelated, _companyId).FirstOrDefault();
                if (infoFactura == null)
                {
                    item.ValidationMessage = "Error: UUIDRelated no localizado en GP.";
                    item.IsPaymentValid = false;
                }
                item.DocNumber = GenerarConsecutivoSiExisteEtiqueta(infoFactura.DocNumber);              
                item.Amount = infoFactura.Amount;
            }
            catch (Exception e)
            {
                item.ValidationMessage = "Error: Relacionado con la consulta a GP, intente de nuevo.";
            }
        }

        private string GenerarConsecutivoSiExisteEtiqueta(string docnumber)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                var valido = false;
                var index = 0;
                var doc = docnumber;
                do
                {
                    var validadorResponse = new ValidadorCfdiReference.RecepcionCfdiClient()
                        .cfdiPorEtiquetav33(
                            contrato: _contratoApiProdigia,
                            usuario: _usuarioApiProdigia,
                            passwd: _passwordApiProdigia,
                            etiqueta: doc);
                    xmlDoc.LoadXml(validadorResponse);
                    XmlNodeList root = xmlDoc.GetElementsByTagName("consultaRecepcion");
                    XmlNodeList comprobante = xmlDoc.GetElementsByTagName("comprobante");

                    var transaccionOk = ((XmlElement)root[0]).GetElementsByTagName("consultaOk")[0].InnerText;
                    Boolean.TryParse(transaccionOk, out valido);

                    if (valido && comprobante.Count == 0)
                        return doc;
                    
                    if(!valido)
                        return "";

                    index++;
                    doc = string.Format("{0}{1}", docnumber, "_" + index);
                } while (true);
            }
            catch (Exception e)
            {
                throw new Exception();
            }
        }

        private void ValidarXmlApi(string pagoXmlBase64, CFDIProvider item)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                var valido = false;
                Int16 codigo = 0;
                var validadorResponse = new ValidadorCfdiReference.RecepcionCfdiClient()
                    .recepcionCfdi(
                        contrato: _contratoApiProdigia,
                        usuario: _usuarioApiProdigia,
                        passwd: _passwordApiProdigia,
                        cfdiXml: pagoXmlBase64,
                        etiqueta: item.DocNumber,
                        prueba: true,
                        //TODO: Obtener el correo correspondiente para el envío del error
                        opciones: new string[] { "ENVIAR_ERRORES:comprobantesdepagos@caffenio.com," });

                xmlDoc.LoadXml(validadorResponse);
                XmlNodeList root = xmlDoc.GetElementsByTagName("servicioRecepcion");
                XmlNodeList resultado = xmlDoc.GetElementsByTagName("resultado");

                //Manejo de errores en la recepción independiente de la validación dedl XML
                var transaccionOk = ((XmlElement)root[0]).GetElementsByTagName("recepcionOk")[0].InnerText;
                Boolean.TryParse(transaccionOk, out valido);
                if (!valido)
                {
                    var errorGral = ((XmlElement)root[0]).GetElementsByTagName("mensaje")[0].InnerText;
                    throw new Exception(errorGral);
                }

                item.UUID = ((XmlElement) resultado[0]).GetAttribute("uuid");
                item.ValidationMessage = ((XmlElement) resultado[0]).GetAttribute("mensaje");
                var itemIsPaymentValid = false;
                Boolean.TryParse(((XmlElement)resultado[0]).GetAttribute("valido"), out itemIsPaymentValid);
                item.IsPaymentValid = itemIsPaymentValid;
            }
            catch (Exception e)
            {
                item.IsPaymentValid = false;
                item.ValidationMessage = e.Message;
            }
        }
       
    }
}
