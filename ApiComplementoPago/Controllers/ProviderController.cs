﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ApiComplementoPago.Models;
using Domain;

namespace ApiComplementoPago.Controllers
{
    //[EnableCors(origins: "http://10.1.5.130", headers: "*", methods: "*")]
    public class ProviderController : ApiController
    {
        /// <summary>
        /// Valida que los datos proporcionados por el proveedor sean reales al momento del registro de usuario.
        /// </summary>
        /// <param name="pModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ProviderModel Validate(ProviderModel pModel)
        {
            var proveedor = new ProviderModel();
            try
            {
                using (ComplementoPagoDBEntities db = new ComplementoPagoDBEntities())
                {
                    proveedor = db.ProveedorPorRfcObtener(pModel.RFC, pModel.CompanyId).Select(r => new ProviderModel
                    {
                        ProviderId = r.ProviderId,
                        ProviderName = r.ProviderName,
                        RFC = r.RFC,
                        Email = r.Email,
                        CompanyId = r.CompanyId
                    }).FirstOrDefault();
                    if(proveedor != null)
                        proveedor.CompanyId = pModel.CompanyId;
                }
            }
            catch (Exception e)
            {
                proveedor = pModel;
            }
            
            return proveedor;
        }


      

        /// <summary>
        /// Regresa listado de proveedores registrados con el RFC proporcionado en una compañía en
        /// específico, para el envío de la invitación
        /// </summary>
        /// <param name="RFC"></param>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<ProviderModel> GetbyRFC(string RFC, string CompanyId)
        {
            IEnumerable<ProviderModel> proveedor;
            try
            {
                using (ComplementoPagoDBEntities db = new ComplementoPagoDBEntities())
                {
                    proveedor = db.ProveedorPorRfcObtener(RFC, CompanyId).Select(r => new ProviderModel
                    {
                        ProviderId = r.ProviderId,
                        ProviderName = r.ProviderName,
                        RFC = r.RFC,
                        Email = r.Email,
                    }).ToList();
                }
            }
            catch (Exception e)
            {
                proveedor = null;
            }
            return proveedor;
        }
    }
}
