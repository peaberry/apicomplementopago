﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using System.Web.Http.Cors;
using ApiComplementoPago.Models;
using Domain;

namespace ApiComplementoPago.Controllers
{
    //[EnableCors(origins: "http://10.1.5.130", headers: "*", methods: "*")]
    public class CaffenioCompaniesController : ApiController
    {
        /// <summary>
        /// Consulta listado de compañías disponibles que pertenecen al corporativo CAFFENIO para llenar
        /// información en un DropDownList y poder seleccionar compañía
        /// </summary>
        /// <returns>listado de companias activas</returns>
        [HttpGet]
        public IEnumerable<CaffenioCompanies> Get()
        {
            var lstCompanies = new List<CaffenioCompanies>();
            try
            {
                using (ComplementoPagoDBEntities db = new ComplementoPagoDBEntities())
                {
                    lstCompanies = db.EmpresasGpObtener().AsEnumerable().Select(x => new CaffenioCompanies
                    {
                        CompanyId = x.CompanyId,
                        CompanyName = x.CompanyName
                    }).ToList();
                }
            }
            catch (Exception e)
            {
                lstCompanies = new List<CaffenioCompanies>();
            }

            return lstCompanies;
        }
    }
}
