﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Domain;
using Domain.Repositorio;

namespace ApiComplementoPago.Controllers
{
    //[EnableCors(origins: "http://10.1.5.130", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        private ComplementoPagoData accesData;
        /// <summary>
        /// Retorna si el usuario es valido al momento de la consulta, para dar acceso a la aplicacion 'Complemento de pago'
        /// </summary>
        /// <param name="providerId">identificador del provedor</param>
        /// <param name="companyId">identificador de la empresa</param>
        /// <returns>True o False</returns>
        [HttpGet]
        public bool Validate(string providerId, string companyId)
        {
            bool valido = false;
            try
            {
                using (ComplementoPagoDBEntities db = new ComplementoPagoDBEntities())
                {
                    var queryResult = db.ProveedorGpValidar(providerId, companyId).FirstOrDefault();
                    Boolean.TryParse(queryResult.ToString(), out valido);
                }
            }
            catch (Exception e)
            {
                valido = false;
            }

            return valido;
        }
    }
}
