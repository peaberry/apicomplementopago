﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiComplementoPago.Models
{
    public class CFDIProvider
    {
        public string DocNumber { get; set; }
        public string UUID { get; set; }
        public decimal Amount { get; set; }
        public string UUIDRelated { get; set; }
        public decimal PaymentAmount { get; set; }
        public bool IsPaymentValid { get; set; }
        public bool IsPaymentComplete { get; set; }
        public string ValidationMessage { get; set; }

        //Campos auxiliares
        public string Moneda { get; set; }
        

    }
}