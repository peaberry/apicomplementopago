﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiComplementoPago.Models
{
    public class ProviderModel
    {
        public string ProviderId { get; set; }
        public string ProviderName { get; set; }
        public string RFC { get; set; }
        public string Email { get; set; }

        //TODO:Agregar info al modelo 

        public  string CompanyId { get; set; }
    }
}