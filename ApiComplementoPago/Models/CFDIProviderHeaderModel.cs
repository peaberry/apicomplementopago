﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiComplementoPago.Models
{
    public class CFDIProviderHeaderModel
    {
        public string ProviderId { get; set; }
        public string CompanyId { get; set; }

        public List<CFDIPaymentDetailModel> Payments { get; set; }
    }

    public class CFDIPaymentDetailModel
    {
        public string UUIDRelated { get; set; }
        public decimal PaymentAmount { get; set; }
        public String XMLBase64 { get; set; }
        public string Moneda { get; set; }

    }
}