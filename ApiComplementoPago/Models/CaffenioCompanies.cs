﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiComplementoPago.Models
{
    public class CaffenioCompanies
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
    }
}